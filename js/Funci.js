/*Manejo de Arrays */

//declaracion de array con elementos enteros

let arreglo = [4,89,30,10,34,89,10,5,8,28];

//diseñar una funcion que recibe como argumento un arreglo 
//de enteros e imprime cada elemento y el tamaño del arreglo
function mosContenido(arreglo){

    for(let i=0; i<arreglo.length; i++){    //Se muestra arreglo completo
        console.log(arreglo[i]);
    }
    console.log("-------------");

}
mosContenido(arreglo);

//funcion para mostrar el promedio de los elementos de array
function mosPromedio(arreglo){
    let promedio=0;
    let n=0;

    for(let i=0; i<arreglo.length; i++){
        n += arreglo[i]; 
    }
    promedio = n/arreglo.length;
    console.log(promedio);
    console.log("-------------");

}
mosPromedio(arreglo);

//funcion para mostrar los valores pares de un arreglo
function mosPares(arreglo){

    for(let i=0; i<arreglo.length; i++){
        if(arreglo[i]%2==0){
            console.log(arreglo[i]);
        }
    }
    console.log("-------------");

}
mosPares(arreglo);

//funcion para mostrar el valor mayor de los elementos de un arreglo
function valorMayor(arreglo){
    let mayor=0;
    for(let x=0;x<arreglo.length;x++){
        if(arreglo[x]>mayor){
            mayor = arreglo[x];
        }    
    }
    console.log("Mayor: "+mayor);
    console.log("-------------");
}
console.log(valorMayor(arreglo));

//funcion para llenar con valores aleatorios el arreglo
function valorAlea(arreglo){
    let Aleatorio = [];
    let mayor =0;
    for(let x=0;x<100;x++){
        Aleatorio[x] = Math.round((Math.random()*100).toFixed(2));
    }
    for(let x=0;x<100;x++){
        
    }
    console.log("Arreglo aleatorio: ");
    console.log(Aleatorio);
    return Aleatorio;
    
}
console.log("-------------");
valorAlea(arreglo);

//funcion que muestra el valor menor y la posicion del arreglo
function valorMenor(arreglo){
    let menor=arreglo[0];
    for(let x=0;x<arreglo.length;x++){
        if(arreglo[x]<menor){
            menor = arreglo[x];
        }    
    }
    console.log("Menor: "+menor);
    console.log("-------------");
}
console.log(valorMenor(arreglo));
