/* Declarar Variables */

const btnCalcular= document.getElementById('btnCalcular');

btnCalcular.addEventListener("click", function(){
    //Obtener los datos de los input

    let valorAuto = document.getElementById('ValorAuto').value;
    let pInicial = document.getElementById('porcentaje').value;
    let plazos = document.getElementById('plazos').value;
    let parrafo = document.getElementById('pa');


    //Hacer los Calculos
    let pagoInicia = valorAuto *(pInicial/100);
    let totalFin = valorAuto-pagoInicia;
    let pagoMensual = totalFin/plazos;

    //Mostrar los datos
    document.getElementById('pagoInicial').value = pagoInicia;
    document.getElementById('totalfin').value = totalFin;
    document.getElementById('pagoMensual').value = pagoMensual;

    let registro = document.createElement('p');
    let salto = document.createElement('br');
    let linea = document.createElement('hr');

    registro.innerHTML = "Valor Auto: "+valorAuto+", Porcentaje"+pInicial+", Plazos:"+plazos
    +", pago Inicial: "+ pagoInicia+ ", Total fin: "+ totalFin+ ", Mensualidad: "+pagoInicia;

    let nuev = document.getElementById('Nuevo');

    nuev.appendChild(registro);
    nuev.appendChild(salto);
    nuev.appendChild(linea);

});



function arribaMouse(){

    parrafo = document.getElementById('pa');
    parrafo.style.color = "#336699"
    parrafo.style.fontSize = '25px';
    parrafo.style.textAlign = "justify";

}

function salirMouse(){

    let parrafo = document.getElementById('pa');
    parrafo.style.color = "red";
    parrafo.style.fontSize = '17px';
    parrafo.style.textAlign = "center";

}

function limpiar(){

    document.getElementById('pagoInicial').value = null;
    document.getElementById('totalfin').value = null;
    document.getElementById('pagoMensual').value = null;  

    document.getElementById('ValorAuto').value = null;
    document.getElementById('porcentaje').value = null;
    document.getElementById('plazos').value = null;

}

