const datTabla = document.getElementById('datos');
var imagen = document.getElementById('imagen');
var img = document.createElement('img');

let alumno = [{

    "Matricula": "2020030847",
    "Nombre": "Gonzalez Luna Nizarindany",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto":'<img src ="/img/2020030847.jpg">',

},
{

    "Matricula": "2020030550",
    "Nombre": "Flores Perez Jesus Adolfo",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/2020030550.jfif">',

},
{

    "Matricula": "2020030604",
    "Nombre": "Plazola Arangure Johan Alek",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/2020030604.jfif">',

},
{

    "Matricula": "2021030077",
    "Nombre": "Arias Tirado Mateo",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/202103077.enc">',

},
{

    "Matricula": "2021030328",
    "Nombre": "Garcia Gonzalez Jorge Enrique",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/2021030328.jfif">',

},
{

    "Matricula": "2021030266",
    "Nombre": "Gonzalez Ramirez Jose Manuel",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/2021030266.jfif">',

},
{

    "Matricula":"2020030714",
    "Nombre":"Ruiz Guerrero Axel Jovani",
    "Grupo":"7-3",
    "Carrera":"Tecnologias de la Informacion",
    "Foto":'<img src="/img/2020030714.jfif">',

},
{
    
    "Matricula": "2021030143",
    "Nombre": "Ibarra Hernandez Angel",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/2021030143.jfif">',

},
{
    
    "Matricula": "2020030321",
    "Nombre": "Ontiveros Govea Yair Alejandro ",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src ="/img/2020030321.jpg">',

},
{

    "Matricula": "2021030262",
    "Nombre": "Qui Mora Ángel Ernesto",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src ="/img/2021030262.jpg">',

},
];

for(let j=0; j<alumno.length; j++){

    console.log("Matricula" + alumno[j].Matricula);
    console.log("Nombre: " + alumno[j].Nombre);
    console.log("Grupo: " + alumno[j].Grupo);
    console.log("Carrera: " + alumno[j].Carrera);
    console.log("Foto: " + alumno[j].Foto);
    console.log("-----------------------------");

}

/*
console.log("Matricula: " + alumno.Matricula);
console.log("Nombre: " + alumno.Nombre);

alumno.Nombre = "Nizarindany Luna";
console.log("Nuevo Nombre: " + alumno.Nombre);
*/

//Objetos Compuestos

let cuentaBanco = {

    "numero": "10001",
    "banco": "BANCOMER",
    cliente: {
        "Nombre": "Jose Madero",
        "fechaNac": "1980/30/11",
        "Sexo": "M"

    },

    "saldo": "10000"
}

console.log("Nombre: " + cuentaBanco.cliente.Nombre);
console.log("Saldo: " + cuentaBanco.saldo);
cuentaBanco.cliente.Sexo = 'F';
console.log(cuentaBanco.cliente.Sexo);

//arreglo de productos

let productos = [
    {
    "codigo":"1001",
    "descripcion":"Atun",
    "precio":"34"},
    {
    "codigo":"1002",
    "descripcion":"Jabon",
    "precio":"23"},
    {
    "codigo":"1003",
    "descripcion":"Harina",
    "precio":"43"},
    {
    "codigo":"1004",
    "descripcion":"Pasta dental",
    "precio":"78"}
]

//Mostrar un atributo de un objeto en un arreglo
/*
console.log("La descripcion es " + productos[0].descripcion);

for(let i=0; i<productos.length; i++){

    console.log("Codigo: " + productos[i].codigo);
    console.log("Descripcion: " + productos[i].descripcion);
    console.log("Precio: " + productos[i].precio);
    console.log("------------------------------");

}
*/
  
for(let i=0; i<alumno.length; i++){

    let fila = datTabla.insertRow();
    let celda = fila.insertCell(0);
    let celda1 = fila.insertCell(1);
    let celda2 = fila.insertCell(2);
    let celda3 = fila.insertCell(3);
    let celda4 = fila.insertCell(4);

    celda.innerHTML = alumno[i].Matricula;
    celda1.innerHTML = alumno[i].Nombre;
    celda2.innerHTML = alumno[i].Grupo;
    celda3.innerHTML = alumno[i].Carrera;
    celda4.innerHTML = alumno[i].Foto;

}
