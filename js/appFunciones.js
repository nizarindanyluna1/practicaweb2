
const btnLlenar = document.getElementById('btnLlenar');
const btnLimpiar = document.getElementById('btnLimpiar');

btnLlenar.addEventListener("click", function(){

    var arreglo = [];
    var impar=0;
    var par=0;

    let numGen = document.getElementById('numGen').value;
    arreglo = numAlea(arreglo, numGen);
    arreglo.sort();
    opc(arreglo);
    simetria(arreglo, impar, par);

});

btnLimpiar.addEventListener("click", function(){

    var numA = document.getElementById("numAlea");
    let pares = document.getElementById('porcentajepa');
    let impares = document.getElementById('porcentajeimpa');
    let ver = document.getElementById('verificador');
    
    numA.innerHTML=" ";
    pares.innerHTML="Porcentaje de pares: ";
    impares.innerHTML="Porcentaje de impares: ";
    ver.innerHTML=" ";
    document.getElementById('numGen').value=" ";
    var opt = document.createElement("option");
    opt.text = "";
    numA.add(opc);

});

function numAlea(arreglo, numGen){

    for(let i=0; i<numGen; i++){
        arreglo[i] = Math.round(Math.random()*100);

    }
    
    return arreglo;
    

}

function opc(arreglo){
    var numA = document.getElementById("numAlea");
    
    for(let i=0; i<arreglo.length; i++){
        var opt = document.createElement("option");
        opt.text = arreglo[i];
        opt.value = arreglo[i].toString();
        numA.add(opt);
    }
    
}

function simetria(arreglo,impar,par){

    var total;
    for(let i=0; i<arreglo.length; i++){
        if(arreglo[i]%2==0)par++;
        if(arreglo[i]%2==1)impar++;
    }

    total = par + impar;
    par = par*100/total;
    impar = impar*100/total;
    let pares = document.getElementById('porcentajepa');
    let impares = document.getElementById('porcentajeimpa');
    let ver = document.getElementById('verificador');

    pares.innerHTML="Porcentaje de pares: " + par + "%";
    impares.innerHTML="Porcentaje de impares: " + impar + "%";
    if((par >= 45 && par <= 55) || (impar >= 45 && impar <= 55)){
        ver.innerHTML = "Es simetrico...";
    }else{
        ver.innerHTML = "NO es simetrico...";
    }

}