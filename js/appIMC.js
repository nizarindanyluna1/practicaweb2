/* Declarar Variables */

const btnCalcular= document.getElementById('btnCalcular');
const btnLimpiar= document.getElementById('btnLimpiar');
const datTabla = document.getElementById('calo');

btnCalcular.addEventListener("click", function(){

    let estatura = document.getElementById('Estatura').value;
    let peso = document.getElementById('Peso').value;
    let edad = document.getElementById('Edad').value;
    let variable = document.querySelector('input[name="genero"]:checked');

    //Hacer Calculo
    let totalimc = peso/(estatura*estatura);

    //Mostrar Datos
    document.getElementById('totalimc').value = totalimc.toFixed(2);

    if(totalimc<18.5){
        document.getElementById('estado').value = "Bajo peso";
    }
    else if(totalimc>=18.5 && totalimc<24.9){
        document.getElementById('estado').value = "Peso saludable";
    }
    else if(totalimc>=25.0 && totalimc<29.9){
        document.getElementById('estado').value = "Sobrepeso";
    }
    else if(totalimc>=30.0 && totalimc<34.9){
        document.getElementById('estado').value = "Obesidad tipo I";
    }
    else if(totalimc>=35.0 && totalimc<39.9){
        document.getElementById('estado').value = "Obesidad tipo II";
    }
    else if(totalimc>=40.0){
        document.getElementById('estado').value = "Obesidad tipo III";
    }

    imag(totalimc);
    let calorias = aparte(variable,edad,peso);
    meterTabla(edad,variable,calorias);

});

btnLimpiar.addEventListener("click", function(){

    imagen.innerHTML=" ";
    document.getElementById('estado').value="";
    document.getElementById('totalimc').value="";
    document.getElementById('Estatura').value="";
    document.getElementById('Peso').value="";
    document.getElementById('Edad').value="";

});

function imag(totalimc){

    var imagen = document.getElementById('imagen');
    var img = document.createElement('img');

    if(totalimc<18.5){
        img.src="/img/01.png";
    }
    else if(totalimc>=18.5 && totalimc<24.9){
        img.src="/img/02.png";
    }
    else if(totalimc>=25.0 && totalimc<29.9){
        img.src="/img/03.png";
    }
    else if(totalimc>=30.0 && totalimc<34.9){
        img.src="/img/04.png";
    }
    else if(totalimc>=35.0 && totalimc<39.9){
        img.src="/img/05.png";
    }
    else if(totalimc>=40.0){
        img.src="/img/06.png";
    }

    imagen.innerHTML=" ";
    imagen.appendChild(img);
    
}

function aparte(variable,edad,peso){
    
    var calorias = 0;

    if(edad >= 10 && edad <18){
        if(variable.value == 'masculino'){
            calorias = (17.686 * peso) + 658.2;
        }
        else{
            calorias = (13.384 * peso) + 692.6;
        }
    }

    else if(edad >= 18 && edad <30){
        if(variable.value == 'masculino'){
            calorias = (15.057 * peso) + 692.2;
        }
        else{
            calorias = (14.818 * peso) + 486.6;
        }
    }
    else if(edad >= 30 && edad <60){
        if(variable.value == 'masculino'){
            calorias = (11.472 * peso) + 873.1;
        }
        else{
            calorias = (8.126 * peso) + 845.6;
        }
    }
    else if(edad >= 60){
        if(variable.value == 'masculino'){
            calorias = (11.711 * peso) + 587.7;
        }
        else{
            calorias = (9.082 * peso) + 658.5;
        }
    }

    return calorias;
}

function meterTabla(edad,variable,calorias) {

    
    let datTabla = document.getElementById('calo');

    let fila = datTabla.insertRow();
    let celda = fila.insertCell(0);
    let celda1 = fila.insertCell(1);
    let celda2 = fila.insertCell(2);

    celda.innerHTML = edad;
    celda1.innerHTML = variable.value;
    celda2.innerHTML = calorias.toFixed(2);

}